apk add wget && apk add zip
wget https://services.gradle.org/distributions/gradle-6.8.3-bin.zip
unzip -d /opt/gradle gradle-6.8.3-bin.zip
rm gradle-6.8.3-bin.zip
apk add openjdk11
docker login -p exfire-project-root-password -u exfire